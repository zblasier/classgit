json.array!(@users) do |user|
  json.extract! user, :id, :user_name, :user_phone
  json.url user_url(user, format: :json)
end
